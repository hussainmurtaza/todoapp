﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Data;
using TestApp.Models.Entity;

namespace TestApp.Util
{
    public class TrackUtil
    {
        public static void Add(ApplicationDbContext context,string colName, EventType type,int objectId,TableName tableName,string userId,string value, bool isCommit = false)
        {
            context.AuditLogs.Add(new AuditLog()
            {
                ColumnName = colName,
                CreatedAt = DateTime.Now,
                EventType = (int)type,
                OriginalId = objectId,
                Table = (int)tableName,
                UpdatedBy = userId,
                Value = value,
            });

            if (isCommit)
            {
                context.SaveChanges();
            }
        }
    }
}
