﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using TestApp.Models;

namespace TestApp.Util
{
    public class JwtTokenUtil
    {
        public const string Secret = "this is my custom Secret key for authnetication";
        public static string Create(string email)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,email),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
            };
            var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
            var token = new JwtSecurityToken(
                issuer: "http://www.google.com",
                audience: "http://www.google.com",
                expires: DateTime.UtcNow.AddDays(2),
                claims: claims,
                signingCredentials: new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256)
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public static string Create(string email, List<Claim> claims)
        {

            var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
            var token = new JwtSecurityToken(
                issuer: "http://www.google.com",
                audience: "http://www.google.com",
                expires: DateTime.UtcNow.AddDays(2),
                claims: claims,
                signingCredentials: new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256)
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public static async Task<List<Claim>> GetValidClaims(ApplicationUser user, UserManager<ApplicationUser> userManager,RoleManager<IdentityRole> roleManager)
        {
            var _options = new IdentityOptions();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(_options.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                new Claim(_options.ClaimsIdentity.UserNameClaimType, user.UserName)
            };
            var userClaims = await userManager.GetClaimsAsync(user);
            var userRoles = await userManager.GetRolesAsync(user);
            claims.AddRange(userClaims);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = await roleManager.FindByNameAsync(userRole);
                if (role != null)
                {
                    var roleClaims = await roleManager.GetClaimsAsync(role);
                    foreach (Claim roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }
            return claims;
        }
    }
}
