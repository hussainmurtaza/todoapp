﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TestApp.CustomValidation;
using TestApp.Models.Entity;

namespace TestApp.Models.ItemViewModels
{
    public class ItemModelViewModel
    {
        [Required(ErrorMessage = "Item Name is required"),MaxLength(100,ErrorMessage = "Item cannot be greater than 100 characters")]
        [RegularExpression(@"^[a-zA-Z0-9\s]{1,}", ErrorMessage = "No Special Characters Allowed")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Price is required"), Range(1, 3000, ErrorMessage = "Range is Between 1 , 3000")]
        public float Price { get; set; }
        public int Id { get; set; }
        [Range(0,3), Required]
        public int ProductType { get; set; }
        [Required, MinValueValidation(1)]
        public int BrandId { get; set; }

        public List<Brand> Brands { get; set; }
    }


    public class ItemTestModelViewModel
    {
        public bool IsChecked { get; set; }

        [RequiredIf(nameof(IsChecked), true)]
        public string Data { get; set; }
    }
}
