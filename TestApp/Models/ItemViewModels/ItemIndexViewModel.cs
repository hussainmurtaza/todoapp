﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Models.Entity;

namespace TestApp.Models.ItemViewModels
{
    public class ItemIndexViewModel
    {
        public IEnumerable<Item> Items { get; set; }
    }
}
