﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.Models.BrandViewModel
{
    public class BrandModelViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
