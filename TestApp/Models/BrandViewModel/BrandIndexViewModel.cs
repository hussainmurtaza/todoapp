﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Models.Entity;

namespace TestApp.Models.BrandViewModel
{
    public class BrandIndexViewModel
    {
        public IEnumerable<Brand> Brands { get; set; }
    }
}
