﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Models.Entity;

namespace TestApp.Models.TrackViewModel
{
    public class TrackIndexViewModel
    {
        public IEnumerable<AuditLog> AuditLogs { get; set; }
    }
}
