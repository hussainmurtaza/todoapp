﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.Models.Entity
{
    public class Brand : Base
    {
        public string Name { get; set; }
        public List<Item> Items { get; set; }


        public override string ToString()
        {
            return Name;
        }
    }
}
