﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.Models.Entity
{
    public class AuditLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        [Range(0,2, ErrorMessage = "Cannot be less than 0 and Greater than 2")]
        public int EventType { get; set; }

        [Range(0,0, ErrorMessage = "Value Not in Range")]
        public int Table { get; set; }
        [Required]
        public string ColumnName { get; set; }
        public int OriginalId { get; set; }
        public string UpdatedBy { get; set; }
        [ForeignKey("UpdatedBy")]
        public ApplicationUser UpdatedByUser { get; set; }
        public string Value { get; set; }
    }

    public enum EventType
    {
        Create = 0,
        Delete = 1,
        Update = 2
    }


    public enum TableName
    {
        Item = 0,
        Brand = 1
    }

}
