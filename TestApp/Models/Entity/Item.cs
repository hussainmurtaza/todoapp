﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace TestApp.Models.Entity
{
    public class Item : Base
    {
        [RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        [Required]
        [StringLength(100, ErrorMessage = "Item name cannot be longer than 50 characters.")]
        public string Name { get; set; }
        [Range(1,float.MaxValue, ErrorMessage = "Price should be greater than 0")]
        public float Price { get; set; }
        [Range(0,3)]
        public int ProductType { get; set; }

        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public Brand Brand { get; set; }

    }

    public enum ProductType
    {
        Electornic = 0,
        Clothes = 1,
        Food = 2,
        Other = 3,
    }
}
