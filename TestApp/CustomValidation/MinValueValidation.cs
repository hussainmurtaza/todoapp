﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.CustomValidation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MinValueValidation : ValidationAttribute
    {
        public MinValueValidation(double propertyValue)
        {
            PropertyValue = propertyValue;
        }

        public override bool RequiresValidationContext => true;
        public double PropertyValue { get; }


        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                var myValue = (int)value;
                if (myValue < PropertyValue)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
                if (myValue >= PropertyValue)
                {
                    return ValidationResult.Success;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
