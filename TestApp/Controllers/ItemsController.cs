﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TestApp.Data;
using TestApp.Models;
using TestApp.Models.Entity;
using TestApp.Models.ItemViewModels;

namespace TestApp.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        public ItemsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, ILogger<ItemsController> logger)
        {
            _context = context;
            _userManager = userManager;
            _logger = logger;
        }

        // GET: Items
        public IActionResult Index()
        {
            _logger.LogInformation("Displaying Item/Index Page to User : " + _userManager.GetUserId(User));
            var applicationDbContext = _context.Items.Where(p => !p.IsDeleted).Include(p => p.Brand);
            return View(new ItemIndexViewModel()
            {
                Items = applicationDbContext.ToList()
            });
        }

        public IActionResult Test()
        {
            var model = new ItemTestModelViewModel();
            return View(model);
        }
        [HttpPost]
        public IActionResult TestPost([FromForm] ItemTestModelViewModel model)
        {
            if (ModelState.IsValid)
            {

            }
            else
            {
            }
            return View("Test",model);
        }
        // GET: Items/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                _logger.LogWarning("Item Not Foun to User : " + _userManager.GetUserId(User));
                return NotFound();
            }
            _logger.LogInformation("Displaying Item/Details Page to User : " + _userManager.GetUserId(User));

            var item = await _context.Items
                .SingleOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            var model = new ItemModelViewModel()
            {
                Brands = _context.Brands.Where(p => !p.IsDeleted).ToList()
            };
            return View(model);
        }
        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ItemModelViewModel item)
        {
            if (ModelState.IsValid)
            {
                var id = _userManager.GetUserId(User);
                var myItem = new Item()
                {
                    CreatedAt = DateTime.Now,
                    IsDeleted = false,
                    Name = item.Name,
                    Price = item.Price,
                    UpdatedAt = DateTime.Now,
                    BrandId = item.BrandId,
                    ProductType = item.ProductType
                };
                _context.Items.Add(myItem);
                await _context.SaveChangesAsync();
                _context.Add(new AuditLog()
                {
                    CreatedAt = DateTime.Now,
                    UpdatedBy = id,
                    EventType = (int)EventType.Create,
                    ColumnName = nameof(myItem.Name),
                    Table = (int)TableName.Item,
                    OriginalId = myItem.Id,
                    Value = myItem.Name
                });
                _context.Add(new AuditLog()
                {
                    CreatedAt = DateTime.Now,
                    UpdatedBy = id,
                    EventType = (int)EventType.Create,
                    ColumnName = nameof(myItem.Price),
                    Table = (int)TableName.Item,
                    OriginalId = myItem.Id,
                    Value = myItem.Price + ""
                });
                await _context.SaveChangesAsync();
                _logger.LogInformation("Item Created By User : " + _userManager.GetUserId(User));

                return RedirectToAction(nameof(Index));
            }
            item.Brands = _context.Brands.Where(p => !p.IsDeleted).ToList();
            return View(item);
        }

        // GET: Items/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items.SingleOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return View(new ItemModelViewModel()
            {
                Price = item.Price,
                Name = item.Name,
                Id = item.Id,
                Brands = _context.Brands.ToList(),
                ProductType = item.ProductType,
                BrandId = item.BrandId
            });
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ItemModelViewModel item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = _userManager.GetUserId(User);
                    var isUpdate = false;
                    var myItem = _context.Items.FirstOrDefault(p => p.Id == id);
                    var list = new List<AuditLog>();
                    if (myItem.Price != item.Price)
                    {
                        isUpdate = true;
                        myItem.Price = item.Price;
                        list.Add(new AuditLog()
                        {
                            UpdatedBy = userId,
                            EventType = (int) EventType.Update,
                            ColumnName = nameof(myItem.Price),
                            CreatedAt = DateTime.Now,
                            OriginalId = myItem.Id,
                            Table = (int) TableName.Item,
                            Value = myItem.Price + ""
                        });
                    }
                    if (myItem.Name != item.Name)
                    {
                        isUpdate = true;
                        myItem.Name = item.Name;
                        list.Add(new AuditLog()
                        {
                            UpdatedBy = userId,
                            EventType = (int) EventType.Update,
                            ColumnName = nameof(myItem.Name),
                            CreatedAt = DateTime.Now,
                            OriginalId = myItem.Id,
                            Table = (int) TableName.Item,
                            Value = myItem.Name + ""
                        });
                    }
                    if (myItem.BrandId != item.BrandId)
                    {
                        isUpdate = true;
                        myItem.BrandId = item.BrandId;
                        list.Add(new AuditLog()
                        {
                            UpdatedBy = userId,
                            EventType = (int)EventType.Update,
                            ColumnName = nameof(myItem.BrandId),
                            CreatedAt = DateTime.Now,
                            OriginalId = myItem.Id,
                            Table = (int)TableName.Item,
                            Value = myItem.BrandId + ""
                        });
                    }
                    if (myItem.ProductType != item.ProductType)
                    {
                        isUpdate = true;
                        myItem.ProductType = item.ProductType;
                        list.Add(new AuditLog()
                        {
                            UpdatedBy = userId,
                            EventType = (int)EventType.Update,
                            ColumnName = nameof(myItem.ProductType),
                            CreatedAt = DateTime.Now,
                            OriginalId = myItem.Id,
                            Table = (int)TableName.Item,
                            Value = myItem.ProductType + ""
                        });
                    }
                    if (isUpdate)
                    {
                        myItem.UpdatedAt = DateTime.Now;
                        _context.Items.Update(myItem);
                        _context.AuditLogs.AddRange(list);
                        await _context.SaveChangesAsync();

                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        // GET: Items/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .SingleOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userId = _userManager.GetUserId(User);
            var item = await _context.Items.SingleOrDefaultAsync(m => m.Id == id);
            item.IsDeleted = true;
            item.UpdatedAt = DateTime.Now;
            _context.Items.Update(item);
            _context.AuditLogs.Add(new AuditLog()
            {
                EventType = (int) EventType.Delete,
                ColumnName = "Row",
                CreatedAt = DateTime.Now,
                OriginalId = item.Id,
                Table = (int) TableName.Item,
                UpdatedBy = userId,
                Value = ""
            });
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemExists(int id)
        {
            return _context.Items.Any(e => e.Id == id);
        }

        
    }

}
