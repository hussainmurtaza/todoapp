﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestApp.Data;
using TestApp.Models;
using TestApp.Models.BrandViewModel;
using TestApp.Models.Entity;
using TestApp.Util;

namespace TestApp.Controllers
{
    public class BrandController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public BrandController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Brand
        public IActionResult Index()
        {
            var m = new BrandIndexViewModel()
            {
                Brands = _context.Brands.Where(p => !p.IsDeleted).ToList()
            };
            return View(m);
        }

        // GET: Brand/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brand = await _context.Brands
                .SingleOrDefaultAsync(m => m.Id == id && !m.IsDeleted);
            if (brand == null)
            {
                return NotFound();
            }

            return View(brand);
        }

        // GET: Brand/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Brand/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BrandModelViewModel model)
        {
            if (ModelState.IsValid)
            {
                var id = _userManager.GetUserId(User);
                var brand = new Brand { Name = model.Name };
                brand.UpdatedAt = brand.CreatedAt = DateTime.Now;
                _context.Add(brand);
                await _context.SaveChangesAsync();
                TrackUtil.Add(_context, nameof(brand.Name), EventType.Create, brand.Id, TableName.Brand, id, brand.Name, true);

                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }
        // GET: Brand/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brand = await _context.Brands.SingleOrDefaultAsync(m => m.Id == id);
            if (brand == null)
            {
                return NotFound();
            }
            return View(new BrandModelViewModel()
            {
                Id = brand.Id,
                Name = brand.Name
            });
        }

        // POST: Brand/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BrandModelViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = _userManager.GetUserId(User);
                    var brand = _context.Brands.FirstOrDefault(p => p.Id == id);
                    if (brand.Name != model.Name)
                    {
                        brand.Name = model.Name;
                        brand.UpdatedAt = DateTime.Now;
                        _context.Update(brand);
                        TrackUtil.Add(_context, nameof(brand.Name), EventType.Update, brand.Id, TableName.Brand, userId, brand.Name);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BrandExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Brand/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brand = await _context.Brands
                .SingleOrDefaultAsync(m => m.Id == id);
            if (brand == null)
            {
                return NotFound();
            }

            return View(brand);
        }

        // POST: Brand/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var brand = await _context.Brands.SingleOrDefaultAsync(m => m.Id == id);
            brand.UpdatedAt = DateTime.Now;
            var userId = _userManager.GetUserId(User);
            _context.Update(brand);
            TrackUtil.Add(_context, "ROW", EventType.Update, brand.Id, TableName.Brand, userId, "");
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool BrandExists(int id)
        {
            return _context.Brands.Any(e => e.Id == id);
        }
    }
}
