﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TestApp.Data;
using TestApp.Models.TrackViewModel;

namespace TestApp.Controllers
{
    [Authorize]
    public class TrackController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        public TrackController(ApplicationDbContext context, ILogger<TrackController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var model = new TrackIndexViewModel()
            {
                AuditLogs = _context.AuditLogs.Include(p => p.UpdatedByUser).ToList()
            };

            return View(model);
        }
    }
}
