﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using TestApp.Data;
using TestApp.Models;

namespace TestApp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        public const string Secret = "this is my custom Secret key for authnetication";
        public AuthController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }
        [Route("login2")]
        [HttpPost]
        public async Task<IActionResult> Login2([FromForm] LoginViewModel parameter)
        {
            var user = await _userManager.FindByEmailAsync(parameter.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, parameter.Password))
            {
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub,user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };
                var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
                var token = new JwtSecurityToken(
                    issuer: "http://www.google.com",
                    audience: "http://www.google.com",
                    expires: DateTime.UtcNow.AddDays(2),
                    claims: claims,
                    signingCredentials: new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256)
                );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo.ToString("G"),
                });
            }
            return Unauthorized();
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginViewModel parameter)
        {
            var user = await _userManager.FindByEmailAsync(parameter.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, parameter.Password))
            {
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub,user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };
                var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
                var token = new JwtSecurityToken(
                    issuer: "http://www.google.com",
                    audience: "http://www.google.com",
                    expires: DateTime.UtcNow.AddDays(2),
                    claims: claims,
                    signingCredentials: new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256)
                );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo.ToString("G"),
                });
            }
            return Unauthorized();
        }

        [Route("get")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new
            {
                result = "Success It Works"
            });
        }
    }

    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}