﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestApp.Data;
using TestApp.Models.Entity;

namespace TestApp.Controllers.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ItemController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public ItemController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            try
            {
                if (id > 0)
                {
                    Item list = _applicationDbContext.Items.FirstOrDefault(p => p.Id == id);
                    if (list != null)
                    {
                        return Ok(new
                        {
                            message = "Success",
                            result = list
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return BadRequest();
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                IEnumerable<Item> list = _applicationDbContext.Items.ToList();
                return Ok(new
                {
                    message = "Success",
                    result = list
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return BadRequest();
        }
    }
}