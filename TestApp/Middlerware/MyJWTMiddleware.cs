﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace TestApp.Middlerware
{
    public class MyJwtMiddleware : IMiddleware
    {
        public Task InvokeAsync(HttpContext context, RequestDelegate next)

        {
            return next.Invoke(context);
        }

        

    }

    public static class JwtMiddlewareExtenstion
    {
        public static IApplicationBuilder UseJwtMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MyJwtMiddleware>();
        }
    }

}
